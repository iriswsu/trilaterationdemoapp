//
//  ViewController.m
//  testingTriangulation
//
//  Created by Jeff Wenzbauer on 4/1/15.
//  Copyright (c) 2015 Jwenz723. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// When the button to calculate the trilateration is clicked
- (IBAction)trilaterationButton:(id)sender {
    // convert point A to floats
    CGFloat pAx = (CGFloat)[self.pointAx.text floatValue];
    CGFloat pAy = (CGFloat)[self.pointAy.text floatValue];
    CGFloat pAdist = (CGFloat)[self.pointAdist.text floatValue];
    
    // convert point B to floats
    CGFloat pBx = (CGFloat)[self.pointBx.text floatValue];
    CGFloat pBy = (CGFloat)[self.pointBy.text floatValue];
    CGFloat pBdist = (CGFloat)[self.pointBdist.text floatValue];
    
    // convert point C to floats
    CGFloat pCx = (CGFloat)[self.pointCx.text floatValue];
    CGFloat pCy = (CGFloat)[self.pointCy.text floatValue];
    CGFloat pCdist = (CGFloat)[self.pointCdist.text floatValue];
    
    // Convert the floats into CGPoints
    CGPoint point1 = CGPointMake(pAx, pAy);
    CGPoint point2 = CGPointMake(pBx, pBy);
    CGPoint point3 = CGPointMake(pCx, pCy);
    
    // Calculate the trilaterated point
    CGPoint currentlocation = [self getPointFromTrilateration:point1 beaconB:point2 beaconC:point3 distanceA:pAdist distanceB:pBdist distanceC:pCdist];
    
    // Display the trilaterated point
    self.finalPoint.text = [NSString stringWithFormat:@"%f, %f", currentlocation.x, currentlocation.y];
}

// When the button to calculate the distance from the path is clicked
- (IBAction)distanceFromPathButton:(id)sender {
    // convert the user entered values to CGFloats
    CGFloat pathStartX = (CGFloat)[self.pathStartX.text floatValue];
    CGFloat pathStartY = (CGFloat)[self.pathStartY.text floatValue];
    CGFloat pathEndX = (CGFloat)[self.pathEndX.text floatValue];
    CGFloat pathEndY = (CGFloat)[self.pathEndY.text floatValue];
    CGFloat currentLocationX = (CGFloat)[self.currentLocationX.text floatValue];
    CGFloat currentLocationY = (CGFloat)[self.currentLocationY.text floatValue];
    
    // Convert the CGFloats to CGPoints
    CGPoint pathStart = CGPointMake(pathStartX, pathStartY);
    CGPoint pathEnd = CGPointMake(pathEndX, pathEndY);
    CGPoint currentLocation = CGPointMake(currentLocationX, currentLocationY);
    
    // Calculate the distance from the path
    CGFloat distance = [self getDistanceFromPath:pathStart pathEnd:pathEnd currentLocation:currentLocation];
    
    // Display the calculated distance
    self.distanceFromPath.text = [NSString stringWithFormat:@"%f", distance];
}


/*! This function calculates how far off of a path a certain point is.  The distance is calculated as if you drew a line going through the "currentLocation" point and perpendicularly intersecting with the line that is created by connecting "pathStart" to "pathEnd".
 \param pathStart: the point where the path starts
 \param pathEnd: the point where the path ends
 \param currentLocation: the point where the distance will be calculated to
 \returns The float value distance from the path
 */
- (CGFloat)getDistanceFromPath:(CGPoint)pathStart pathEnd:(CGPoint)pathEnd currentLocation:(CGPoint)currentLocation {

    // shift the path start point to be at 0,0
    CGFloat shiftX = pathStart.x;
    CGFloat shiftY = pathStart.y;
    pathStart.x -= shiftX;
    pathStart.y -= shiftY;
    
    // shift the other 2 points the same amount
    pathEnd.x -= shiftX;
    pathEnd.y -= shiftY;
    currentLocation.x -= shiftX;
    currentLocation.y -= shiftY;
    
    // find the slope of the path line.  There is no need to subtract the pathStart point because it is (0,0)
    CGFloat m = pathEnd.y/pathEnd.x;
    
    // Find the slope of the line perpendicular to the path
    CGFloat inverseM = -(1/m);
    
    // Find the point where the inverse line crosses the Y Axis
    CGFloat inverseB = currentLocation.y - (inverseM * currentLocation.x);
    
    // Find the point where the inverse line intersects the path line
    CGFloat x = inverseB / (m - inverseM);
    CGFloat y = m * x;
    
    // Calculate the distance that the new point is from the current location
    CGFloat d = powf((powf(currentLocation.x - x, 2.0) + powf(currentLocation.y - y, 2.0)), 0.5);
    
    return d;
}


/*! This function uses trilateration to determine the coordinates of an unknown point using the coordinates of 3 points and the distance of each point to the unknown point.
 \param bA: one of the points/iBeacons in the trilateration triangle
 \param bB: one of the points/iBeacons in the trilateration triangle
 \param bC: one of the points/iBeacons in the trilateration triangle
 \param distanceA: distance from point bA to the desired point
 \param distanceB: distance from point bB to the desired point
 \param distanceC: distance from point bC to the desired point
 \returns The CGPoint that was calculated through trilateration
 */
- (CGPoint)getPointFromTrilateration:(CGPoint)bA beaconB:(CGPoint)bB beaconC:(CGPoint)bC distanceA:(CGFloat)dA distanceB:(CGFloat)dB distanceC:(CGFloat)dC {
    
    // shift point A to be at 0,0
    CGFloat shiftX = bA.x;
    CGFloat shiftY = bA.y;
    bA.x -= shiftX;
    bA.y -= shiftY;
    
    // shift points B and C the same amount that A was shifted
    bB.x -= shiftX;
    bB.y -= shiftY;
    bC.x -= shiftX;
    bC.y -= shiftY;
    
    CGFloat a = 2 * bC.x;
    CGFloat b = 2 * bC.y;
    CGFloat c = 2 * bB.x;
    CGFloat d = 2 * bB.y;
    
    CGFloat m = -(powf(dC, 2.0) - powf(bC.x, 2.0) - powf(bC.y, 2.0) - powf(dA, 2.0));
    CGFloat n = -(powf(dB, 2.0) - powf(bB.x, 2.0) - powf(bB.y, 2.0) - powf(dA, 2.0));
    
    CGFloat x = (d*m - b*n) / (a*d - b*c);
    CGFloat y = (n - c * x) / d;
    
    x += shiftX;
    y += shiftY;
    
    return CGPointMake(x, y);
}


@end

//
//  ViewController.h
//  testingTriangulation
//
//  Created by Mac Attack on 4/1/15.
//  Copyright (c) 2015 Jwenz723. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

// properties used for the form that calculates the trilaterational point
@property (weak, nonatomic) IBOutlet UITextField *pointAx;
@property (weak, nonatomic) IBOutlet UITextField *pointAy;
@property (weak, nonatomic) IBOutlet UITextField *pointAdist;
@property (weak, nonatomic) IBOutlet UITextField *pointBx;
@property (weak, nonatomic) IBOutlet UITextField *pointBy;
@property (weak, nonatomic) IBOutlet UITextField *pointBdist;
@property (weak, nonatomic) IBOutlet UITextField *pointCx;
@property (weak, nonatomic) IBOutlet UITextField *pointCy;
@property (weak, nonatomic) IBOutlet UITextField *pointCdist;
@property (weak, nonatomic) IBOutlet UILabel *finalPoint;


// Properties used for the form that calculates distance from the path
@property (weak, nonatomic) IBOutlet UITextField *pathStartX;
@property (weak, nonatomic) IBOutlet UITextField *pathStartY;
@property (weak, nonatomic) IBOutlet UITextField *currentLocationX;
@property (weak, nonatomic) IBOutlet UITextField *currentLocationY;
@property (weak, nonatomic) IBOutlet UITextField *pathEndX;
@property (weak, nonatomic) IBOutlet UITextField *pathEndY;
@property (weak, nonatomic) IBOutlet UILabel *distanceFromPath;


@end


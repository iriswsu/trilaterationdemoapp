# What are future students supposed to do with this repo? #

The code in this repo should be incorporated into the IRIS app in future semesters.  The 2 main methods in this repo will be helpful in determining if the user being guided is still walking on the correct path.  It will also be useful in determining a user's current location when the user is surrounded by iBeacons.

# What does the code in this repo do? #

This app was created simply as a demo of trilateration.

###There are two functional methods in this app:###
* getDistanceFromPath() - this method receives 3 points as parameters then draws a line between two of the points and calculates how far the third point is off of the line.  This is to be used to determine how far a user has strayed from a navigable path in the IRIS app.

* getPointFromTrilateration() - This method receives 3 points and 3 distances as parameters.  The 3 points represent the coordinate points of 3 beacons that are surrounding a user that is using the IRIS app.  The 3 distances are the distance that each beacon is away from the user.  Using these parameters a 4th point is returned.  This 4th point is the coordinate of where the user is standing.

# Who created this? #

* Jeff Wenzbauer - jwenz723@gmail.com